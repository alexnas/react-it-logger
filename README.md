This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# React IT Logger

This React Redux application is designed to log operations of IT-department technicians. Connected through API with JsonServer as a backend data storage at the dev mode.

## Uses:

- React
- Redux
- React Router
- Redux Thunk middleware
- JsonServer through API
- Materialize-Css

## Installation and Usage

- Be sure that Git and Node.js are installed globally.
- Clone this repo.
- Run `npm install`, all required components will be installed automatically.
- Run `npm dev` to start the project with JsonServer support at the development.
- Run `npm start` to start the project without JsonServer.
- Run `npm build` to create a build directory with a production build of the app.
- Run `npm test` to test the app.

## License

This project is licensed under the terms of the MIT license.
